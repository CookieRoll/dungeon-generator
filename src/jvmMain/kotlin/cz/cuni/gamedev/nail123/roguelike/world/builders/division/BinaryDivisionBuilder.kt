package cz.cuni.gamedev.nail123.roguelike.world.builders.division

import cz.cuni.gamedev.nail123.roguelike.GameConfig
import cz.cuni.gamedev.nail123.roguelike.blocks.Floor
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Door
import cz.cuni.gamedev.nail123.roguelike.extensions.toIntArray2D
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder
import org.hexworks.zircon.api.data.Position3D
import org.hexworks.zircon.api.data.Size3D

class BinaryDivisionBuilder (size: Size3D = GameConfig.AREA_SIZE, visibleSize: Size3D = GameConfig.VISIBLE_SIZE) : AreaBuilder(size, visibleSize) {


    override fun create(): AreaBuilder = apply {
        var roomRoot = BinaryRoom(Position3D.create(0,0,0), Position3D.create(width-1, height-1, 0))
        roomRoot.Split(1.0) // always do at least one split
        roomRoot.AddCorridors() // add corridors

        // draw the rooms
        roomRoot.Draw(blocks)
        // and the corridors
        roomRoot.DrawCorridors(blocks, 0)
    }


}