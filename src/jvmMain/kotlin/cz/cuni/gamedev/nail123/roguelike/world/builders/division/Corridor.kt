package cz.cuni.gamedev.nail123.roguelike.world.builders.division

import cz.cuni.gamedev.nail123.roguelike.blocks.Floor
import cz.cuni.gamedev.nail123.roguelike.blocks.GameBlock
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.utils.collections.ObservableMap
import org.hexworks.zircon.api.data.Position3D

class Corridor (var leftTopCorner: Position3D, var rightBotCorner: Position3D, var splitHor: Boolean) {
    fun GetWidth() : Int {
        return rightBotCorner.x - leftTopCorner.x + 1
    }
    fun GetHeight() : Int {
        return rightBotCorner.y - leftTopCorner.y + 1
    }

    fun Draw(blocks: ObservableMap<Position3D, GameBlock>) {
        // draw a corridor
        for (x in 0 until GetWidth()) {
            for (y in 0 until GetHeight()) {
                // decide where the border lies based on orientation
                val isBorder = (splitHor && (x == 0 || x == GetWidth() - 1)) || (!splitHor && (y == 0 || y == GetHeight() - 1))
                blocks[Position3D.create(leftTopCorner.x + x, leftTopCorner.y + y, 0)] = if (isBorder) Wall() else Floor()
            }
        }
        //print("Drawn a corridor from " + leftTopCorner + " to " + rightBotCorner + "\n")
    }

}