package cz.cuni.gamedev.nail123.roguelike.world.worlds

import cz.cuni.gamedev.nail123.roguelike.GameConfig
import cz.cuni.gamedev.nail123.roguelike.blocks.Floor
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Stairs
import cz.cuni.gamedev.nail123.roguelike.entities.unplacable.FogOfWar
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.world.Area
import cz.cuni.gamedev.nail123.roguelike.world.builders.wavefunctioncollapse.WFCAreaBuilder
import org.hexworks.zircon.api.data.Position3D
import kotlin.math.abs

class WaveFunctionCollapsedWorld: DungeonWorld() {
    override fun buildLevel(floor: Int): Area {
        print("\n...\n")
        val area = WFCAreaBuilder(GameConfig.AREA_SIZE).create()

        area.addAtEmptyPosition(
            area.player,
            Position3D.create(0, 0, 0),
            GameConfig.VISIBLE_SIZE
        )

        print("\n...\n")
        print(area.player.position)
        print("...\n\n")

        // connect areas
        fixConnectivity(area, area.player.position)

        //area.addEntity(FogOfWar(), Position3D.unknown())

        // Add stairs up
        if (floor > 0) area.addEntity(Stairs(false), area.player.position)

        // Add stairs down
        val floodFill = Pathfinding.floodFill(area.player.position, area)
        val maxDistance = floodFill.values.maxOrNull()!!
        val staircasePosition = floodFill.filter { it.value > maxDistance / 2 }.keys.random()
        area.addEntity(Stairs(), staircasePosition)
        print("\n...\n")
        return area.build()
    }

    private fun fixConnectivity(area: WFCAreaBuilder, playerPos : Position3D) {
        var floodFill = Pathfinding.floodFill(area.player.position, area)
        // go through entire map
        var mapPoints = area.allPositions.filter{area[it]?.blocksMovement == false && !floodFill.containsKey(it)}

        while (mapPoints.count() > 0){
            print("\n Testing mappoint " + mapPoints.first() + "\n")
            // take first point and connect it
            //tunnel(area, Pathfinding.floodFill(mapPoints.first(), area).keys.random(), playerPos)
            connectRoom(area, playerPos, mapPoints.first())
            print("map points before" + mapPoints.count())
            floodFill = Pathfinding.floodFill(area.player.position, area)
            mapPoints = mapPoints.filter { !floodFill.containsKey(it) }
        print("map points after " + mapPoints.count())
        }
    }

    private fun connectRoom(area: WFCAreaBuilder, playerPos: Position3D, testPos : Position3D){
        var newPos = testPos
        print("\nTunneling begin.")
        while(true) {
            print("\nTunneling from " + newPos + "\n")
            var tmp = findWall(area, newPos, playerPos)
            print("\nTunneling to " + tmp + "\n")
            //tmp = tunnel(area, tmp, playerPos)

            //print("\nTunneling3 from " + tmp + "\n")

            val points = Pathfinding.floodFill(testPos, area)

            //check if player reachable
            if (points.containsKey(playerPos)) break

            val maxDistance = points.values.maxOrNull()!!
            newPos = points.filter { it.value >= maxDistance }.keys.random()
        }
        print("\nTunneling ends.")
    }

    private fun findWall(area: WFCAreaBuilder, pos: Position3D, playerPos: Position3D) : Position3D{
        var diffX = playerPos.x - pos.x
        var diffY = playerPos.y - pos.y

        var newPos = pos

        if ((abs(diffX) < abs(diffY)) ) {
            // player is further on y-axis
            if (diffY > 0) {
                // player is bellow point
                while(area.blocks[newPos] != null && !area.blocks[newPos]!!.blocksMovement)
                    newPos = Position3D.create(newPos.x, newPos.y + 1, 0)
                newPos = Position3D.create(newPos.x, newPos.y - 1, 0)
            } else {
                // player is above point
                while(area.blocks[newPos] != null && !area.blocks[newPos]!!.blocksMovement)
                    newPos = Position3D.create(newPos.x, newPos.y - 1, 0)
                newPos = Position3D.create(newPos.x, newPos.y + 1, 0)
            }
            //find edges and pick random spot from them
            val list = ArrayList<Position3D>()
            // go all the way left
            while(true){
                val testPos = Position3D.create(newPos.x-1, newPos.y, 0)
                if(area.blocks[testPos] == null || area.blocks[testPos]!!.blocksMovement)
                    break
                else
                    newPos = testPos
            }
            // now all the way right
            while(area.blocks[newPos] != null && !area.blocks[newPos]!!.blocksMovement){
                list.add(newPos)
                newPos = Position3D.create(newPos.x+1, newPos.y, 0)
            }
            newPos = list.random()

            print("\n Tunneling from " + newPos)

            if(diffY>0) newPos = tunnelVer(area, newPos, 1)
            else newPos = tunnelVer(area, newPos, -1)
        } else {
            // player is further on x-axis
            if (diffX > 0) {
                // player is right of point
                while(area.blocks[newPos] != null && !area.blocks[newPos]!!.blocksMovement)
                    newPos = Position3D.create(newPos.x + 1, newPos.y, 0)
                newPos = Position3D.create(newPos.x - 1, newPos.y, 0)
            } else {
                // player is left of point
                while(area.blocks[newPos] != null && !area.blocks[newPos]!!.blocksMovement)
                    newPos = Position3D.create(newPos.x - 1, newPos.y, 0)
                newPos = Position3D.create(newPos.x + 1, newPos.y, 0)
            }
            //find edges and pick random spot from them
            val list = ArrayList<Position3D>()
            // go all the way up
            while(true){
                val testPos = Position3D.create(newPos.x, newPos.y-1, 0)
                if(area.blocks[testPos] == null || area.blocks[testPos]!!.blocksMovement)
                    break
                else {
                    newPos = testPos
                }
            }
            // now all the way down
            while(area.blocks[newPos] != null && !area.blocks[newPos]!!.blocksMovement){
                list.add(newPos)
                newPos = Position3D.create(newPos.x, newPos.y+1, 0)
            }
            newPos = list.random()

            print("Tunneling through " + newPos)

            if(diffX>0) newPos = tunnelHor(area, newPos, 1)
            else newPos = tunnelHor(area, newPos, -1)
        }

        return newPos
    }

    private fun tunnelHor(area: WFCAreaBuilder, pos: Position3D, incr: Int) : Position3D{
        var newPos = pos

        do {
            newPos = Position3D.create(newPos.x + incr, newPos.y, 0)
            if (area.blocks[newPos] != null && area.blocks[newPos]!!.blocksMovement)
                area.blocks[newPos] = Floor()
        } while (area.blocks[newPos] != null && area.blocks[newPos]!!.blocksMovement)

        return newPos
    }

    private fun tunnelVer(area: WFCAreaBuilder, pos: Position3D, incr: Int) : Position3D{
        var newPos = pos

        do {
            newPos = Position3D.create(newPos.x, newPos.y + incr, 0)
            if (area.blocks[newPos] != null && area.blocks[newPos]!!.blocksMovement)
                area.blocks[newPos] = Floor()
        } while (area.blocks[newPos] != null && area.blocks[newPos]!!.blocksMovement)

        return newPos
    }

    private fun tunnel(area: WFCAreaBuilder, pos : Position3D, playerPos: Position3D) : Position3D{
        var diffX = playerPos.x - pos.x
        var diffY = playerPos.y - pos.y


        var newPos = pos

        while(true){
            if ((abs(diffX) > abs(diffY) && diffY != 0) || diffX == 0) {
                // player is closer on y-axis
                if (diffY > 0) {
                    // player is bellow point
                    newPos = Position3D.create(newPos.x, newPos.y + 1, 0)
                } else {
                    // player is above point
                    newPos = Position3D.create(newPos.x, newPos.y - 1, 0)
                }
            } else {
                // player is closer on x-axis
                if (diffX > 0) {
                    // player is right of point
                    newPos = Position3D.create(newPos.x + 1, newPos.y, 0)
                } else {
                    // player is left of point
                    newPos = Position3D.create(newPos.x - 1, newPos.y, 0)

                }
            }

            if (area.blocks[newPos] == null || !area.blocks[newPos]!!.blocksMovement) break
            else {
                area.blocks[newPos] = Floor()
            }
        }
        return newPos
    }
}