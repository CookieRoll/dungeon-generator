package cz.cuni.gamedev.nail123.roguelike.world.builders.division

import cz.cuni.gamedev.nail123.roguelike.blocks.Floor
import cz.cuni.gamedev.nail123.roguelike.blocks.GameBlock
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.utils.collections.ObservableMap
import org.hexworks.zircon.api.data.Position
import org.hexworks.zircon.api.data.Position3D
import kotlin.random.Random

class BinaryRoom(var leftTopCorner: Position3D, var rightBotCorner: Position3D) {
    val minRoomDim = 5 // the minimal size a room can be in any dimension, don't slice beyond this point
    val chanceRed = 0.05
    val cornerMargin = 1
    val minCorridor = 1

    var leftRoom : BinaryRoom? = null
    var rightRoom : BinaryRoom? = null

    var corridor : Corridor? = null

    var horSplit = false
    var verSplit = false

    fun GetWidth() : Int {
        return rightBotCorner.x - leftTopCorner.x + 1
    }
    fun GetHeight() : Int {
        return rightBotCorner.y - leftTopCorner.y + 1
    }

    fun IsLeaf() : Boolean {
        return !horSplit && !verSplit
    }

    fun Split(splitChance: Double) {
        if (Math.random() > splitChance)
            return
        // attempt a random  split
        if (Math.random() < 0.5 && GetHeight() >= 2 * minRoomDim) {
            // do horizontal split
            HorizontalSplit(splitChance - chanceRed)
        } else if (GetWidth() >= 2 * minRoomDim) {
            // do vertical split
            VerticalSplit(splitChance - chanceRed)
        } else if (GetHeight() >= 2 * minRoomDim) {
            // if we randomly skip the hor split, check if it might be possible if ver isnt
            HorizontalSplit(splitChance - chanceRed)
        }
    }

    private fun HorizontalSplit(splitChance: Double) {
        // split the room horizontally into two rooms
        var border = Random.nextInt(minRoomDim-1, GetHeight()-minRoomDim)


        leftRoom = BinaryRoom(Position3D.create(leftTopCorner.x, leftTopCorner.y, 0), Position3D.create(rightBotCorner.x, leftTopCorner.y + border, 0))
        rightRoom = BinaryRoom(Position3D.create(leftTopCorner.x, leftTopCorner.y + border + 1, 0), Position3D.create(rightBotCorner.x, rightBotCorner.y, 0))

        horSplit = true

        leftRoom?.Split(splitChance)
        rightRoom?.Split(splitChance)
    }

    private fun VerticalSplit(splitChance: Double) {
        // split the room vertically into two rooms
        var border = Random.nextInt(minRoomDim-1, GetWidth()-minRoomDim)

        leftRoom = BinaryRoom(Position3D.create(leftTopCorner.x, leftTopCorner.y, 0), Position3D.create(leftTopCorner.x + border, rightBotCorner.y, 0))
        rightRoom = BinaryRoom(Position3D.create(leftTopCorner.x + border + 1, leftTopCorner.y, 0), Position3D.create(rightBotCorner.x, rightBotCorner.y, 0))

        verSplit = true

        leftRoom?.Split(splitChance)
        rightRoom?.Split(splitChance)
    }

    fun Draw(blocks: ObservableMap<Position3D, GameBlock>){
        if (IsLeaf()) {
            for (x in 0 until GetWidth()) {
                for (y in 0 until GetHeight()) {
                    val isBorder = x == 0 || x == GetWidth() - 1 || y == 0 || y == GetHeight() - 1
                    blocks[Position3D.create(leftTopCorner.x + x, leftTopCorner.y + y, 0)] = if (isBorder) Wall() else Floor()
                }
            }
        } else {
            leftRoom?.Draw(blocks)
            rightRoom?.Draw(blocks)
        }
    }

    fun DrawCorridors(blocks: ObservableMap<Position3D, GameBlock>, corIdx: Int){
        var newIdx = corIdx
        if(corridor !=null)
            newIdx += 1

        leftRoom?.DrawCorridors(blocks, newIdx)
        rightRoom?.DrawCorridors(blocks, newIdx)

        if(corridor != null) {
            //print("\n--Drawing corridor " + newIdx + "\n")
            corridor!!.Draw(blocks)
        }
    }

    fun GetRightWallPoints() : ArrayList<Int> {
        var points = arrayListOf<Int>()

        if(!IsLeaf()){
            if(rightRoom != null)
                points.addAll(rightRoom!!.GetRightWallPoints())
            if (leftRoom != null && horSplit)
                points.addAll(leftRoom!!.GetRightWallPoints())
        } else {
            for (i in cornerMargin until GetHeight()-cornerMargin)
                points.add(leftTopCorner.y + i)
        }

        return points
    }

    fun GetLeftWallPoints() : ArrayList<Int> {
        var points = arrayListOf<Int>()

        if(!IsLeaf()){
            if(leftRoom != null)
                points.addAll(leftRoom!!.GetLeftWallPoints())
            if (rightRoom != null && horSplit)
                points.addAll(rightRoom!!.GetLeftWallPoints())
        } else {
            for (i in cornerMargin until GetHeight()-cornerMargin)
                points.add(leftTopCorner.y + i)
        }

        return points
    }

    fun GetRightWallPointsCommented() : ArrayList<Int> {
        var points = arrayListOf<Int>()

        if(!IsLeaf()){
            print("\nExploring child rooms for right points.\n")
            if(rightRoom != null) {
                print("exploring right room child")
                points.addAll(rightRoom!!.GetRightWallPointsCommented())
            }
            if (horSplit && leftRoom != null) {
                print("exploring left room child")
                points.addAll(leftRoom!!.GetRightWallPointsCommented())
            }
            print("\n----Finished exploring.\n")
        } else {
            for (i in cornerMargin until GetHeight()-cornerMargin)
                points.add(leftTopCorner.y + i)
            print("\n In leaf, added points from " + (leftTopCorner.y + cornerMargin) + " to " + (leftTopCorner.y + GetHeight()-cornerMargin-1) + " at x " + rightBotCorner.x)
        }

        return points
    }

    fun GetLeftWallPointsCommented() : ArrayList<Int> {
        var points = arrayListOf<Int>()

        if(!IsLeaf()){
            print("\nExploring child rooms for left points.\n")
            if(leftRoom != null) {
                print("exploring left room child")
                points.addAll(leftRoom!!.GetLeftWallPointsCommented())
            }
            if (horSplit && rightRoom != null) {
                print("exploring right room child")
                points.addAll(rightRoom!!.GetLeftWallPointsCommented())
            }
            print("\n----Finished exploring.\n")
        } else {
            for (i in cornerMargin until GetHeight()-cornerMargin)
                points.add(leftTopCorner.y + i)
            print("\n In leaf, added points from " + (leftTopCorner.y + cornerMargin) + " to " + (leftTopCorner.y + GetHeight()-cornerMargin-1) + " at x " + leftTopCorner.x)
        }

        return points
    }

    fun GetTopWallPoints() : ArrayList<Int> {
        var points = arrayListOf<Int>()

        if(!IsLeaf()){
            if(leftRoom != null)
                points.addAll(leftRoom!!.GetTopWallPoints())
            if (rightRoom != null && verSplit)
                points.addAll(rightRoom!!.GetTopWallPoints())
        } else {
            for (i in cornerMargin until GetWidth()-cornerMargin)
                points.add(leftTopCorner.x + i)
        }

        return points
    }

    fun GetBotWallPoints() : ArrayList<Int> {
        var points = arrayListOf<Int>()

        if(!IsLeaf()){
            if(rightRoom != null)
                points.addAll(rightRoom!!.GetBotWallPoints())
            if (leftRoom != null && verSplit)
                points.addAll(leftRoom!!.GetBotWallPoints())
        } else {
            for (i in cornerMargin until GetWidth()-cornerMargin)
                points.add(leftTopCorner.x + i)
        }

        return points
    }

    fun GetIntersectionGroups(points: List<Int>) : List<IntArray> {
        var groups = ArrayList<IntArray>()

        var firstPass = true
        var currGroup = IntArray(2) {0}
        for (i in 0 until points.count()) {
            if(firstPass || points[i-1] != points[i]-1) {
                if (!firstPass) {
                    // we stumbled upon a new group, save the old one if its long enough
                    groups.add(currGroup)
                }

                firstPass = false
                currGroup = intArrayOf(points[i], points[i])
            } else
                currGroup[1] += 1
        }

        if (!firstPass) {
            // add the last group
            groups.add(currGroup)
        }

        return groups.filter { it[1] - it[0] > minCorridor }
    }

    fun AddCorridors(){
        if (IsLeaf())
            return
        if(leftRoom != null)
            leftRoom?.AddCorridors()
        if (rightRoom != null)
            rightRoom?.AddCorridors()

        if (leftRoom != null && rightRoom != null) {
            if (horSplit) {
                //print("\n----adding horizontal corridor \n")
                // add corridor between top and bottom rooms
                val points = leftRoom!!.GetBotWallPoints()!!.intersect(rightRoom!!.GetTopWallPoints().toSet())!!.toList()
                val groups = GetIntersectionGroups(points)

                val rndIdx = Random.nextInt(0, groups.size)
                // set down the corridor
                var corrStart = groups[rndIdx][0]
                var corrEnd = groups[rndIdx][1]
                if(corrEnd - corrStart > minCorridor*2) {
                    // try to randomize the size of the corridor if possible
                    corrStart = Random.nextInt(groups[rndIdx][0], groups[rndIdx][1] - minCorridor*2)
                    corrEnd = Random.nextInt(corrStart + minCorridor*2, groups[rndIdx][1]+1)
                }
                corridor = Corridor(
                    Position3D.create(corrStart, leftRoom!!.rightBotCorner.y, 0),
                    Position3D.create(corrEnd, rightRoom!!.leftTopCorner.y, 0),
                    horSplit
                )

            } else {
                // add corridor between left and right rooms
                //print("\n----adding vertical corridor \n")
                val points =
                    leftRoom!!.GetRightWallPoints()!!.intersect(rightRoom!!.GetLeftWallPoints().toSet())!!.toList()
                val groups = GetIntersectionGroups(points)

                val rndIdx = Random.nextInt(0, groups.size)
                // set down the corridor
                var corrStart = groups[rndIdx][0]
                var corrEnd = groups[rndIdx][1]
                if(corrEnd - corrStart > minCorridor*2) {
                    // try to randomize the size of the corridor if possible
                    corrStart = Random.nextInt(groups[rndIdx][0], groups[rndIdx][1] - minCorridor*2)
                    corrEnd = Random.nextInt(corrStart + minCorridor*2, groups[rndIdx][1]+1)
                }
                corridor = Corridor(
                    Position3D.create(leftRoom!!.rightBotCorner.x, corrStart, 0),
                    Position3D.create(rightRoom!!.leftTopCorner.x, corrEnd, 0),
                    horSplit
                )
            }
        }
    }
}